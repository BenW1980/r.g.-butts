package game.peanutpanda.b3c3.enums;

public enum ScreenSize {

    HEIGHT(800), WIDTH(480);

    private final int size;

    ScreenSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}