package game.peanutpanda.b3c3.enums;

public enum Colour {

    RED(1), GREEN(2), BLUE(3), NONE(0);

    private int id;

    Colour(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
