package game.peanutpanda.b3c3.enums;


public enum GameState {

    IN_GAME, GAME_OVER

}
