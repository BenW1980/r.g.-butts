package game.peanutpanda.b3c3.assetloader;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class AssetLoader {

    public final AssetManager assetManager = new AssetManager();

    public void startLoading() {

        loadImages();
        loadSkins();
        loadFontHandling();
        loadAtlas();
        loadSounds();
    }

    public void loadSounds() {
        assetManager.load("sounds/boing.ogg", Sound.class);
        assetManager.load("sounds/death.ogg", Sound.class);
    }

    public void loadImages() {
        assetManager.load("images/blueBar.png", Texture.class);
        assetManager.load("images/redBar.png", Texture.class);
        assetManager.load("images/greenBar.png", Texture.class);
        assetManager.load("images/transBar.png", Texture.class);
        assetManager.load("images/logo.png", Texture.class);

        assetManager.load("images/blueBarPocket.png", Texture.class);
        assetManager.load("images/redBarPocket.png", Texture.class);
        assetManager.load("images/greenBarPocket.png", Texture.class);
        assetManager.load("images/logoPocket.png", Texture.class);
    }

    public void loadSkins() {
        SkinLoader.SkinParameter params = new SkinLoader.SkinParameter("b3c3.atlas");
        assetManager.load("b3c3.json", Skin.class, params);
    }

    public void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter robotoLarge = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        robotoLarge.fontFileName = "fonts/roboto.ttf";
        robotoLarge.fontParameters.size = 75;
        robotoLarge.fontParameters.color = Color.WHITE;
        assetManager.load("robotoLarge.ttf", BitmapFont.class, robotoLarge);

        FreetypeFontLoader.FreeTypeFontLoaderParameter roboto = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        roboto.fontFileName = "fonts/roboto.ttf";
        roboto.fontParameters.size = 50;
        roboto.fontParameters.color = Color.WHITE;
        assetManager.load("roboto.ttf", BitmapFont.class, roboto);
    }

    public void loadAtlas() {
        assetManager.load("animations/blueFlyer.atlas", TextureAtlas.class);
        assetManager.load("animations/greenFlyer.atlas", TextureAtlas.class);
        assetManager.load("animations/redFlyer.atlas", TextureAtlas.class);

        assetManager.load("animations/pocket/blueFlyer.atlas", TextureAtlas.class);
        assetManager.load("animations/pocket/greenFlyer.atlas", TextureAtlas.class);
        assetManager.load("animations/pocket/redFlyer.atlas", TextureAtlas.class);

    }

    public Skin skin() {
        return assetManager.get("b3c3.json", Skin.class);
    }

    public Texture logo() {
        return assetManager.get("images/logo.png", Texture.class);
    }

    public Texture transBar() {
        return assetManager.get("images/transBar.png", Texture.class);
    }

    public Texture blueBar() {
        return assetManager.get("images/blueBar.png", Texture.class);
    }

    public Texture redBar() {
        return assetManager.get("images/redBar.png", Texture.class);
    }

    public Texture greenBar() {
        return assetManager.get("images/greenBar.png", Texture.class);
    }

    public Texture logoPocket() {
        return assetManager.get("images/logoPocket.png", Texture.class);
    }

    public Texture blueBarPocket() {
        return assetManager.get("images/blueBarPocket.png", Texture.class);
    }

    public Texture redBarPocket() {
        return assetManager.get("images/redBarPocket.png", Texture.class);
    }

    public Texture greenBarPocket() {
        return assetManager.get("images/greenBarPocket.png", Texture.class);
    }

    public Sound boing() {
        return assetManager.get("sounds/boing.ogg", Sound.class);
    }

    public Sound gameOver() {
        return assetManager.get("sounds/death.ogg", Sound.class);
    }

}
