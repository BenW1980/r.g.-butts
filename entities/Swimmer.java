package game.peanutpanda.b3c3.entities;


import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;

import game.peanutpanda.b3c3.assetloader.AssetLoader;
import game.peanutpanda.b3c3.enums.Mode;
import game.peanutpanda.b3c3.gamestuffs.Score;
import game.peanutpanda.b3c3.gamestuffs.ScreenShake;

public class Swimmer {

    private float elapsedTime;
    private float speed;
    private int id;
    private boolean hit;
    private AssetLoader assetLoader;
    private Sprite sprite;
    private Rectangle rectangle;
    private TextureAtlas moveAtlas;
    private Animation<TextureRegion> moveAnimation;

    public Swimmer(AssetLoader assetLoader, float moveAnimSpeed, int number, Rectangle rectangle, int speed, Mode mode) {
        this.assetLoader = assetLoader;
        this.sprite = new Sprite();
        this.setColour(number, mode);
        this.moveAnimation = new Animation<TextureRegion>(moveAnimSpeed, moveAtlas.getRegions());
        this.rectangle = rectangle;
        this.speed = speed;
    }

    private void setColour(int id, Mode mode) {

        String atlasLocation = null;

        if (mode == Mode.FULL) {
            switch (id) {
                case 1:
                    atlasLocation = "animations/redFlyer.atlas";
                    break;
                case 2:
                    atlasLocation = "animations/greenFlyer.atlas";
                    break;
                case 3:
                    atlasLocation = "animations/blueFlyer.atlas";
                    break;
            }
        } else {
            switch (id) {
                case 1:
                    atlasLocation = "animations/pocket/redFlyer.atlas";
                    break;
                case 2:
                    atlasLocation = "animations/pocket/greenFlyer.atlas";
                    break;
                case 3:
                    atlasLocation = "animations/pocket/blueFlyer.atlas";
                    break;
            }
        }

        this.moveAtlas = assetLoader.assetManager.get(atlasLocation, TextureAtlas.class);
        this.id = id;
    }

    public void update(float delta, Stage stage) {
        elapsedTime += delta;
        sprite.setRegion(moveAnimation.getKeyFrame(elapsedTime, true));

        rectangle.y -= speed * delta;

        if (hit) {
            sprite.setFlip(true, true);
        }

        stage.getBatch().draw(sprite, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    public void checkCollision(Barrier barrier, Score score, ScreenShake screenShake) {
        if (this.rectangle.overlaps(barrier.getRectangle()) && this.id == barrier.getId() && !hit) {
            assetLoader.boing().play();
            screenShake.shake(0.3f, 1, 35, 60, true);
            score.increase();
            hit = true;
            speed = -speed * 3;
        }
    }

    public boolean swimsAway(Stage stage) {
        return rectangle.y > stage.getHeight() * 3;
    }

    public boolean breaksThrough() {
        return rectangle.y < 0;
    }
}