package game.peanutpanda.b3c3.entities;


import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;

import game.peanutpanda.b3c3.assetloader.AssetLoader;
import game.peanutpanda.b3c3.enums.Mode;

public class Barrier {

    private static int MAX_HEIGHT = 100;
    private static int MIN_HEIGHT = 1;
    private static int SPEED = 1500;
    private AssetLoader assetLoader;
    private Rectangle rectangle;
    private Sprite sprite;
    private int id;

    public Barrier(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
        this.rectangle = new Rectangle(-100, 250, 600, 0);
        this.sprite = new Sprite(assetLoader.transBar());
        this.sprite.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    public void expand(float delta) {
        rectangle.height += delta * SPEED;
        if (rectangle.height > MAX_HEIGHT) {
            rectangle.setHeight(MAX_HEIGHT);
        }
    }

    public void contract(float delta) {
        rectangle.height -= delta * SPEED;
        if (rectangle.height < MIN_HEIGHT) {
            setColour(game.peanutpanda.b3c3.enums.Colour.NONE, Mode.FULL);
            rectangle.setHeight(MIN_HEIGHT);
        }
    }

    public void draw(Stage stage) {
        stage.getBatch().draw(sprite, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    public void setColour(game.peanutpanda.b3c3.enums.Colour colour, Mode mode) {

        if (mode == Mode.FULL) {
            switch (colour) {
                case BLUE:
                    this.sprite.setTexture(assetLoader.blueBar());
                    break;
                case GREEN:
                    this.sprite.setTexture(assetLoader.greenBar());
                    break;
                case RED:
                    this.sprite.setTexture(assetLoader.redBar());
                    break;
                case NONE:
                    this.sprite.setTexture(assetLoader.transBar());
                    break;
            }

        } else {

            switch (colour) {
                case BLUE:
                    this.sprite.setTexture(assetLoader.blueBarPocket());
                    break;
                case GREEN:
                    this.sprite.setTexture(assetLoader.greenBarPocket());
                    break;
                case RED:
                    this.sprite.setTexture(assetLoader.redBarPocket());
                    break;
                case NONE:
                    this.sprite.setTexture(assetLoader.transBar());
                    break;
            }
        }

        this.id = colour.getId();
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public int getId() {
        return id;
    }
}
