package game.peanutpanda.b3c3.screens;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.peanutpanda.b3c3.B3C3;
import game.peanutpanda.b3c3.enums.ScreenSize;


public class LoadingScreen implements Screen {

    private Sprite pandaSprite;
    private Sprite pandaTxtSprite;
    private Sprite loadingBar;
    private Sprite loadingBarBackground;

    private B3C3 game;
    private SpriteBatch batch;

    public LoadingScreen(B3C3 game) {
        this.game = game;
        this.batch = new SpriteBatch();
        this.pandaSprite = new Sprite(new Texture(Gdx.files.internal("loadingAssets/pepaWhite.png")));
        this.pandaTxtSprite = new Sprite(new Texture(Gdx.files.internal("loadingAssets/pepatxt.png")));
        this.loadingBar = new Sprite(new Texture(Gdx.files.internal("loadingAssets/loadingBar.png")));
        this.loadingBarBackground = new Sprite(new Texture(Gdx.files.internal("loadingAssets/loadingBarBackground.png")));
    }

    @Override
    public void show() {
        game.assLoader.startLoading();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(255 / 255f, 255 / 255f, 255 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.camera.update();
        batch.setProjectionMatrix(game.camera.combined);

        batch.begin();

        pandaTxtSprite.setBounds(100, 365, 200, 113);
        pandaTxtSprite.draw(batch);

        pandaSprite.setBounds(330, 365, 75, 113);
        pandaSprite.draw(batch);

        loadingBar.setPosition(ScreenSize.WIDTH.getSize() / 2 - 150, ScreenSize.HEIGHT.getSize() / 2 - 200);
        loadingBar.setSize(game.assLoader.assetManager.getProgress() * 300, 46);
        loadingBarBackground.setPosition(ScreenSize.WIDTH.getSize() / 2 - 150, ScreenSize.HEIGHT.getSize() / 2 - 200);
        loadingBarBackground.setSize(300, 46);
        loadingBar.draw(batch);
        loadingBarBackground.draw(batch);

        batch.end();

        if (game.assLoader.assetManager.update()) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(game));
        }
    }


    @Override
    public void resize(int width, int height) {
        game.viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.batch.dispose();
        this.pandaSprite.getTexture().dispose();
        this.pandaTxtSprite.getTexture().dispose();
        this.loadingBar.getTexture().dispose();
        this.loadingBarBackground.getTexture().dispose();
    }
}
