package game.peanutpanda.b3c3.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.b3c3.assetloader.AssetLoader;
import game.peanutpanda.b3c3.B3C3;
import game.peanutpanda.b3c3.enums.GameState;
import game.peanutpanda.b3c3.gamestuffs.Score;
import game.peanutpanda.b3c3.gamestuffs.ScreenShake;


public abstract class BaseScreen implements Screen {

    Stage stage;
    OrthographicCamera camera;
    Viewport viewport;
    AssetLoader assetLoader;
    B3C3 game;
    Skin skin;
    BitmapFont fntNormal;
    BitmapFont fntLarge;
    ScreenShake screenShake;
    GameState gameState;
    Score score;

    BaseScreen(B3C3 game) {
        this.game = game;
        this.camera = game.camera;
        this.viewport = game.viewport;
        this.stage = new Stage(viewport);
        this.assetLoader = game.assLoader;
        this.skin = assetLoader.skin();
        this.score = new Score();
        this.screenShake = new ScreenShake();
        this.fntNormal = assetLoader.assetManager.get("roboto.ttf", BitmapFont.class);
        this.fntLarge = assetLoader.assetManager.get("robotoLarge.ttf", BitmapFont.class);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(41 / 255f, 41 / 255f, 41 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.act();
        stage.draw();
        screenShake.update(delta, camera);

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        game.assLoader.assetManager.dispose();
    }
}