package game.peanutpanda.b3c3.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import game.peanutpanda.b3c3.B3C3;
import game.peanutpanda.b3c3.enums.Mode;
import game.peanutpanda.b3c3.gamestuffs.Score;

public class GameOverScreen extends BaseScreen {

    private Score score;

    GameOverScreen(final B3C3 game, Score score) {
        super(game);
        Gdx.input.setCatchBackKey(true);
        this.score = score;

        Table rootTable = new Table();
        rootTable.setFillParent(true);

        TextButton againButt = new TextButton("ONE MORE TURN!", skin);
        againButt.getLabel().setFontScale(0.9f);
        TextButton menuButt = new TextButton("MAIN MENU", skin);
        menuButt.getLabel().setFontScale(0.7f);

        if (game.mode == Mode.FULL) {
            againButt.setStyle(skin.get("extra2", TextButton.TextButtonStyle.class));
            menuButt.setStyle(skin.get("normal", TextButton.TextButtonStyle.class));
        } else {
            againButt.setStyle(skin.get("pocket", TextButton.TextButtonStyle.class));
            menuButt.setStyle(skin.get("pocket", TextButton.TextButtonStyle.class));
        }

        againButt.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(game));
            }
        });
        menuButt.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(game));
            }
        });

        final int buttWidth = 300;
        final int buttHeight = 90;

        rootTable.add(againButt).width(buttWidth).height(buttHeight).padTop(340);
        rootTable.row();
        rootTable.add(menuButt).width(buttWidth).height(buttHeight).padTop(40);
        stage.addActor(rootTable);
        assetLoader.gameOver().play();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.getBatch().begin();
        fntLarge.draw(stage.getBatch(), " GAME OVER", 8, stage.getHeight() / 2 + 290);
        fntLarge.draw(stage.getBatch(), "SCORE " + score.getCount(), 60, stage.getHeight() / 2 + 190);
        stage.getBatch().end();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
