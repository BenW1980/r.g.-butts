package game.peanutpanda.b3c3.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import game.peanutpanda.b3c3.B3C3;
import game.peanutpanda.b3c3.enums.GameState;
import game.peanutpanda.b3c3.enums.Mode;
import game.peanutpanda.b3c3.entities.Barrier;
import game.peanutpanda.b3c3.enums.Colour;
import game.peanutpanda.b3c3.entities.Swimmer;

public class GameScreen extends BaseScreen {

    private long lastSpawn;
    private Array<Swimmer> flyBoys;
    private Array<ImageButton> butts;
    private Barrier barrier;
    private boolean buttPushed;
    private float timePassed;
    private long spawnTime;
    private int speed;

    public GameScreen(final B3C3 game) {
        super(game);
        Gdx.input.setCatchBackKey(true);
        gameState = GameState.IN_GAME;
        this.flyBoys = new Array<Swimmer>();
        this.butts = new Array<ImageButton>();
        this.barrier = new Barrier(assetLoader);
        this.spawnTime = 999999000;
        this.speed = 500;

        Table rootTable = new Table();
        rootTable.setFillParent(true);

        if (game.mode == Mode.FULL) {
            butts.addAll(new ImageButton(skin, "red"), new ImageButton(skin, "green"), new ImageButton(skin, "blue"));
        } else {
            butts.addAll(new ImageButton(skin, "redPocket"), new ImageButton(skin, "greenPocket"), new ImageButton(skin, "bluePocket"));
        }

        for (int i = 0; i < butts.size; i++) {

            final int finalI = i;
            butts.get(i).addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    switch (finalI) {
                        case 0:
                            barrier.setColour(Colour.RED, game.mode);
                            break;
                        case 1:
                            barrier.setColour(Colour.GREEN, game.mode);
                            break;
                        case 2:
                            barrier.setColour(Colour.BLUE, game.mode);
                            break;
                    }
                    buttPushed = true;
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    buttPushed = false;
                }
            });
            rootTable.add(butts.get(i)).size(150).padTop(550);
        }
        stage.addActor(rootTable);
    }

    private void createFlyboys() {

        if (TimeUtils.nanoTime() - lastSpawn > spawnTime) {
            this.flyBoys.add(new Swimmer(
                    assetLoader, 1f / 10f,
                    MathUtils.random(1, 3),
                    new Rectangle(MathUtils.random(80, 400), 1600, 50, 130),
                    speed, game.mode));
            lastSpawn = TimeUtils.nanoTime();
        }
    }

    private void increaseDifficulty() {

        if (timePassed > 0.5f) {
            spawnTime -= 10000000;
            speed += 5;
            timePassed = 0;
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        timePassed += delta;

        stage.getBatch().begin();

        switch (gameState) {
            case IN_GAME:
                barrier.draw(stage);

                if (buttPushed) {
                    barrier.expand(delta);
                } else {
                    barrier.contract(delta);
                }

                increaseDifficulty();
                createFlyboys();

                for (Swimmer swimmer : flyBoys) {
                    swimmer.update(delta, stage);
                    swimmer.checkCollision(barrier, score, screenShake);
                    if (swimmer.swimsAway(stage)) {
                        flyBoys.removeValue(swimmer, true);
                    }
                    if (swimmer.breaksThrough()) {
                        gameState = GameState.GAME_OVER;
                    }
                }

                fntNormal.draw(stage.getBatch(), "" + score.getCount(), 25, stage.getHeight() - 25);
                break;

            case GAME_OVER:
                ((Game) Gdx.app.getApplicationListener()).setScreen(new GameOverScreen(game, score));
                break;
        }

        stage.getBatch().end();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
