package game.peanutpanda.b3c3.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import game.peanutpanda.b3c3.B3C3;
import game.peanutpanda.b3c3.enums.Mode;


public class MainMenuScreen extends BaseScreen {

    private Texture logo;
    private TextButton pocketButt;
    private TextButton colorButt;
    private TextButton playButt;
    private String pocketModeActive;
    private String normalModeActive;
    private String pocketModeInactive;
    private String normalModeInactive;

    public MainMenuScreen(final B3C3 game) {
        super(game);

        pocketModeActive = ">  GAME JAM MODE  <";
        normalModeActive = "> FULL COLOR MODE <";
        pocketModeInactive = "   GAME JAM MODE   ";
        normalModeInactive = "  FULL COLOR MODE  ";

        logo = assetLoader.logo();

        Table rootTable = new Table();
        rootTable.setFillParent(true);

        pocketButt = new TextButton(pocketModeInactive, skin);
        pocketButt.getLabel().setFontScale(0.7f);
        colorButt = new TextButton(normalModeActive, skin);
        colorButt.getLabel().setFontScale(0.7f);
        playButt = new TextButton("PLAY GAME", skin, "normal");

        final int buttWidth = 300;
        final int buttHeight = 90;

        switch (game.mode) {
            case FULL:
                setAttrToFull();
                break;
            case POCKET:
                setAttrToPocket();
                break;
        }

        playButt.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(game));
            }
        });

        pocketButt.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                setAttrToPocket();
            }
        });

        colorButt.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                setAttrToFull();
            }
        });

        rootTable.add(pocketButt).width(buttWidth).height(buttHeight).padTop(340);
        rootTable.row();
        rootTable.add(colorButt).width(buttWidth).height(buttHeight);
        rootTable.row();
        rootTable.add(playButt).width(buttWidth).height(buttHeight).padTop(40);
        stage.addActor(rootTable);
    }

    private void setAttrToFull() {
        game.mode = Mode.FULL;
        pocketButt.setStyle(skin.get("extra", TextButton.TextButtonStyle.class));
        colorButt.setStyle(skin.get("extra", TextButton.TextButtonStyle.class));
        playButt.setStyle(skin.get("normal", TextButton.TextButtonStyle.class));
        pocketButt.setText(pocketModeInactive);
        colorButt.setText(normalModeActive);
        logo = assetLoader.logo();
    }

    private void setAttrToPocket() {
        game.mode = Mode.POCKET;
        pocketButt.setStyle(skin.get("pocket", TextButton.TextButtonStyle.class));
        colorButt.setStyle(skin.get("pocket", TextButton.TextButtonStyle.class));
        playButt.setStyle(skin.get("pocket", TextButton.TextButtonStyle.class));
        pocketButt.setText(pocketModeActive);
        colorButt.setText(normalModeInactive);
        logo = assetLoader.logoPocket();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.getBatch().begin();
        stage.getBatch().draw(logo, 80, stage.getHeight() - 240, 320, 206);
        fntLarge.draw(stage.getBatch(), "  R.G.", 140, stage.getHeight() / 2 + 140);
        fntLarge.draw(stage.getBatch(), "BUTTS", 130, stage.getHeight() / 2 + 75);
        stage.getBatch().end();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
