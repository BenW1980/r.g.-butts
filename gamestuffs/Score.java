package game.peanutpanda.b3c3.gamestuffs;

public class Score {

    private int count;

    public void increase() {
        count++;
    }

    public int getCount() {
        return count;
    }
}
