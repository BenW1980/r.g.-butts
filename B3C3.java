package game.peanutpanda.b3c3;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import game.peanutpanda.b3c3.assetloader.AssetLoader;
import game.peanutpanda.b3c3.enums.Mode;
import game.peanutpanda.b3c3.enums.ScreenSize;
import game.peanutpanda.b3c3.screens.LoadingScreen;

public class B3C3 extends Game {

    public AssetLoader assLoader;
    public OrthographicCamera camera;
    public Viewport viewport;
    public Mode mode;

    @Override
    public void create() {
        this.assLoader = new AssetLoader();
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.camera.update();
        this.mode = Mode.FULL;
        setScreen(new LoadingScreen(this));
    }
}
